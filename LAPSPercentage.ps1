#Requires -Modules ActiveDirectory,AdmPwd.PS
$LastLogonTimeframe = [DateTime]::Today.AddDays(-90)
$SearchOU = "OU=Computers,DC=contoso,DC=com"
$Computers = Get-ADComputer -Filter {(enabled -eq "true") -and (OperatingSystem -like "Windows*") -and (LastLogonDate -ge $LastLogonTimeframe)} -SearchBase $SearchOU

$ComputersWithPass = ($Computers | Get-AdmPwdPassword | Where-Object {$_.Password -ne $null}).Count
$TotalComputers = ($Computers).Count
[math]::round(($ComputersWithPass / $TotalComputers) * 100,2)