﻿#Requires -Version 3.0
#Requires -Modules ActiveDirectory, ImportExcel

# Requires the MySQL .Net Connector 6.9.9 be installed (https://dev.mysql.com/downloads/connector/)
# If you use a newer version of the MySQL .NET Connector, update the line that starts with "Add-Type -AssemblyName"


##### Script Variables - Verify these

#    Connection Strings
$Database = "ORG" # Add your ORG number to the end of this variable
$Server = "vk1000shared1.css.udel.edu"
$User = "R" # Add your ORG number to the end of this variable
$Password = "" # Enter your KACE report user password here
# Put the SQL query between the quotes below:
$MySqlQuery = ""

#    Export File
$TodaysDate = Get-Date -UFormat %Y-%m-%d
# You can alter the path below as needed.  This is where the final Excel file will be saved.
$AttachmentPath = "C:\Temp\" + $TodaysDate + "-KACEComplianceReport.xlsx"

#    SMTP Email Settings
$send_email = 0 # Set to 1 if you want it to email you the file using the settings below
$SMTPServer = "mail.udel.edu"
$From = "@udel.edu"
$To = "@udel.edu"
$Subject = "KACE Compliance Report"
$Body = "See attached file."

##### End of Script Variables




#    Import Modules
Try {
#[system.reflection.assembly]::LoadWithPartialName("MySql.Data") | Out-Null
Add-Type -AssemblyName "MySql.Data, Version=6.9.9.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d" -ErrorAction Stop
}
Catch {
"ERROR:  Could not load MySQL .NET Connector"
Exit 1
}


# Preliminaries
$startdate = Get-Date
"Processing started: " + $startdate


# Functions

#    Clean-Text removes <br/>, leading and trailing spaces, and multiple continuous spaces
#    Accepts piped input
Function Clean-Text
{
    $text = $input
    $text = $text.Replace("<br/>", " ")
    $text = $text -replace "^\s+", ""
    $text = $text -replace "\s+\Z", ""
    $text = $text -replace "\s{2,}", " "
    Return $text
}

#    Create-Column creates a new column with a default type of String and places it after a column if specified
#    Format should be:  Create-Column "NewColumnName" "ColumnNameItShouldFollow"(Optional) "NewColumnType"(Optional)
Function Create-Column(
    $NewColumn,
    $AfterColumn = $null,
    $ColumnType = "String"
)
{
    $Column = New-Object System.Data.DataColumn $NewColumn,($ColumnType)
    $DataTable.Columns.Add($Column)
    If ($AfterColumn -ne $null) {
        ($DataTable.Columns[$NewColumn]).SetOrdinal(($DataTable.Columns[$AfterColumn].Ordinal)+1)
    }
}



# Connect to SQL and query data, extract data to DataTable
$MySqlConnection = New-Object MySql.Data.MySqlClient.MySqlConnection
$MySqlConnection.ConnectionString = "SERVER=$Server;DATABASE=$Database;UID=$User;PWD=$Password"
$MySqlConnection.Open()
$MySqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand
$MySqlCmd.CommandText = $MySqlQuery
$MySqlCmd.Connection = $MySqlConnection
$DataTable = New-Object System.Data.DataTable
$DataTable.Load($MySqlCmd.ExecuteReader())
$MySqlConnection.Close()


# Populate Hash Table
$objTable = $DataTable | Select * -ExcludeProperty RowError, RowState, Table, ItemArray, HasErrors


# Export Hash Table to file
#$objTable | Export-CSV $AttachmentPath -NoTypeInformation   --- Could be used instead if you just want a CSV and don't want to use the ImportExcel PowerShell module
If ($send_email -eq 0) {
    $objTable | Export-Excel -Path $AttachmentPath -BoldTopRow -AutoSize -FreezeTopRowFirstColumn -Show
}
Else {
    $objTable | Export-Excel -Path $AttachmentPath -BoldTopRow -AutoSize -FreezeTopRowFirstColumn
}


# Send SMTP Message
If ($send_email -eq 1) {
    Send-MailMessage -To $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -From $From -Attachments $AttachmentPath
}


# Postliminaries
$enddate = Get-Date
"Processing ended:" + $enddate
$scripttime = [math]::Round(($enddate - $startdate).TotalMinutes, 2)
If ($scriptTime -lt 2) {
    $scripttime = [math]::Round(($enddate - $startdate).TotalSeconds, 0)
    "Total script execution time: " + $scripttime + " seconds"
}
Else {
    "Total script execution time: " + $scripttime + " minutes"
}