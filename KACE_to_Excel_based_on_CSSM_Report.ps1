﻿#Requires -Version 3.0
#Requires -Modules ActiveDirectory, ImportExcel

# Requires the MySQL .Net Connector 6.9.9 be installed (https://dev.mysql.com/downloads/connector/)
# If you use a newer version of the MySQL .NET Connector, update the line that starts with "Add-Type -AssemblyName"
# Requires custom inventory fields in KACE for:  AD Distinguished Name, Windows Local Admins, Windows Rogue Local Admins, Windows Local Users,
#     Windows Disabled Local Users, AppLocker Status, Cylance Version, McAfee DAT File Date, Mac Domain, Mac Local Admins, Mac Last User Domain,
#     Identity Finder Windows, Identity Finder Mac, Mac Automatic Update Check, Mac Auto Install App Updates, Mac Auto Install OS X Updates,
#     Windows Local Logon Restricted

# This script expects you're including all the columns found in the Rename Columns section (line 104ish) in your KACE SQL query

##### Script Variables - Verify these
# Also be sure to edit the MACHINE_CUSTOM_INVENTORY numbers in the Rename Columns section of the script as those numbers are unique for each org in KACE.

#    Connection Strings
$Database = "ORG" # Add your ORG number to the end of this variable
$Server = "vk1000shared1.css.udel.edu"
$User = "R" # Add your ORG number to the end of this variable
$Password = "" # Enter your KACE report user password here
# Put the SQL query between the quotes below:
$MySqlQuery = "SELECT MACHINE.NAME AS SYSTEM_NAME, LAST_SYNC, MACHINE.CLIENT_VERSION, MACHINE.ID, MACHINE.MAC, MACHINE.IP, (CONCAT(SUBSTRING_INDEX(UPTIME, ',', 1), ' days, ', SUBSTRING(UPTIME, LOCATE(',', UPTIME) + 1, LOCATE(':', UPTIME) - LOCATE(',', UPTIME) - 1), ' hours, ', SUBSTRING_INDEX(UPTIME, ':', -1), ' minutes')) AS UPTIME, CS_MANUFACTURER, CS_MODEL, CHASSIS_TYPE, BIOS_SERIAL_NUMBER, OS_NAME, OS_VERSION, OS_ARCH, OS_INSTALLED_DATE, CS_DOMAIN, USER_LOGGED, USER_DOMAIN, case ifnull(MACHINE_DRIVE_ENCRYPTION_SUMMARY.IS_DRIVE_ENCRYPTION_ENABLED,0) when 1 then 'True' else 'False' end as IS_DRIVE_ENCRYPTION_ENABLED, MACHINE_DRIVE_ENCRYPTION_SUMMARY.PROTECTION_REASON, case ifnull(MACHINE_DRIVE_ENCRYPTION_SUMMARY.IS_DRIVE_PROTECTED,0) when 1 then 'True' else 'False' end as IS_DRIVE_PROTECTED, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2442) AS MACHINE_CUSTOM_INVENTORY_0_2442, A10002.NAME AS FIELD_10002, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2439) AS MACHINE_CUSTOM_INVENTORY_0_2439, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2432) AS MACHINE_CUSTOM_INVENTORY_0_2432, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2430) AS MACHINE_CUSTOM_INVENTORY_0_2430, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2431) AS MACHINE_CUSTOM_INVENTORY_0_2431, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=16018) AS MACHINE_CUSTOM_INVENTORY_0_16018, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2428) AS MACHINE_CUSTOM_INVENTORY_0_2428, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2429) AS MACHINE_CUSTOM_INVENTORY_0_2429, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=11850) AS MACHINE_CUSTOM_INVENTORY_0_11850, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2440) AS MACHINE_CUSTOM_INVENTORY_0_2440, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=12297) AS MACHINE_CUSTOM_INVENTORY_0_12297, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=16376) AS MACHINE_CUSTOM_INVENTORY_0_16376, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=16378) AS MACHINE_CUSTOM_INVENTORY_0_16378, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2433) AS MACHINE_CUSTOM_INVENTORY_0_2433, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2434) AS MACHINE_CUSTOM_INVENTORY_0_2434, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=2446) AS MACHINE_CUSTOM_INVENTORY_0_2446, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=12535) AS MACHINE_CUSTOM_INVENTORY_0_12535, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=15490) AS MACHINE_CUSTOM_INVENTORY_0_15490, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=15492) AS MACHINE_CUSTOM_INVENTORY_0_15492, (SELECT MACHINE_CUSTOM_INVENTORY.STR_FIELD_VALUE FROM MACHINE_CUSTOM_INVENTORY WHERE MACHINE_CUSTOM_INVENTORY.ID=MACHINE.ID AND MACHINE_CUSTOM_INVENTORY.SOFTWARE_ID=15491) AS MACHINE_CUSTOM_INVENTORY_0_15491  FROM MACHINE  LEFT JOIN MACHINE_DRIVE_ENCRYPTION_SUMMARY ON (MACHINE_DRIVE_ENCRYPTION_SUMMARY.ID = MACHINE.ID)  LEFT JOIN ASSET ON ASSET.MAPPED_ID = MACHINE.ID AND ASSET.ASSET_TYPE_ID=5 LEFT JOIN ASSET_ASSOCIATION J10002 ON J10002.ASSET_ID = ASSET.ID AND J10002.ASSET_FIELD_ID=10002 LEFT JOIN ASSET A10002 ON A10002.ID = J10002.ASSOCIATED_ASSET_ID WHERE ((MACHINE.NAME not like 'CSS-%') AND (OS_NAME not like '%Server%') AND (MACHINE.NAME not like 'PROV-VM-%')) ORDER BY SYSTEM_NAME, LAST_SYNC desc"

#    Export File
$TodaysDate = Get-Date -UFormat %Y-%m-%d
# You can alter the path below as needed.  This is where the final Excel file will be saved.
$AttachmentPath = "C:\Temp\" + $TodaysDate + "-KACEComplianceReport.xlsx"

#    SMTP Email Settings
$send_email = 0 # Set to 1 if you want it to email you the file using the settings below
$SMTPServer = "mail.udel.edu"
$From = "@udel.edu"
$To = "@udel.edu"
$Subject = "KACE Compliance Report"
$Body = "See attached file."

##### End of Script Variables





#    Import Modules
Try {
#[system.reflection.assembly]::LoadWithPartialName("MySql.Data") | Out-Null
Add-Type -AssemblyName "MySql.Data, Version=6.9.9.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d" -ErrorAction Stop
}
Catch {
"ERROR:  Could not load MySQL .NET Connector"
Exit 1
}


# Preliminaries
$creds = Get-Credential -Message "Please enter the credentials you'd like to use for WSUS:"
$startdate = Get-Date
$currentmonth = (Get-Date -UFormat %m) -replace "^0", "" -as [int]
$currentday = (Get-Date -UFormat %d) -replace "^0", "" -as [int]
$currentyear = (Get-Date -UFormat %Y) -as [int]
"Processing started (on " + $startdate + ")"


# Functions

#    Clean-Text removes <br/>, leading and trailing spaces, and multiple continuous spaces
Function Clean-Text
{
    $text = $input
    $text = $text.Replace("<br/>", " ")
    $text = $text -replace "^\s+", ""
    $text = $text -replace "\s+\Z", ""
    $text = $text -replace "\s{2,}", " "
    Return $text
}

#    Create-Column creates a new column with a default type of String and places it after a column if specified
#    Format should be:  Create-Column "NewColumnName" "ColumnNameItShouldFollow"(Optional) "NewColumnType"(Optional)
Function Create-Column(
    $NewColumn,
    $AfterColumn = $null,
    $ColumnType = "String"
)
{
    $Column = New-Object System.Data.DataColumn $NewColumn,($ColumnType)
    $DataTable.Columns.Add($Column)
    If ($AfterColumn -ne $null) {
        ($DataTable.Columns[$NewColumn]).SetOrdinal(($DataTable.Columns[$AfterColumn].Ordinal)+1)
    }
}



# Connect to SQL and query data, extract data to SQL Adapter
$MySqlConnection = New-Object MySql.Data.MySqlClient.MySqlConnection
$MySqlConnection.ConnectionString = "SERVER=$Server;DATABASE=$Database;UID=$User;PWD=$Password"
$MySqlConnection.Open()
$MySqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand
$MySqlCmd.CommandText = $MySqlQuery
$MySqlCmd.Connection = $MySqlConnection
$DataTable = New-Object System.Data.DataTable
$DataTable.Load($MySqlCmd.ExecuteReader())
$MySqlConnection.Close()


# Rename Columns
($DataTable).Columns["SYSTEM_NAME"].ColumnName = "System_Name"
($DataTable).Columns["LAST_SYNC"].ColumnName = "Last_Inventory"
($DataTable).Columns["CLIENT_VERSION"].ColumnName = "KACE_Client_Version"
($DataTable).Columns["MAC"].ColumnName = "MAC_Address"
($DataTable).Columns["IP"].ColumnName = "IP_Address"
($DataTable).Columns["UPTIME"].ColumnName = "Uptime"
($DataTable).Columns["CS_MANUFACTURER"].ColumnName = "Manufacturer"
($DataTable).Columns["CS_MODEL"].ColumnName = "Model"
($DataTable).Columns["CHASSIS_TYPE"].ColumnName = "Chassis_Type"
($DataTable).Columns["BIOS_SERIAL_NUMBER"].ColumnName = "Serial_Number"
($DataTable).Columns["OS_NAME"].ColumnName = "OS_Name"
($DataTable).Columns["OS_ARCH"].ColumnName = "OS_Architecture"
($DataTable).Columns["OS_VERSION"].ColumnName = "OS_Version"
($DataTable).Columns["OS_INSTALLED_DATE"].ColumnName = "OS_Installed_Date"
($DataTable).Columns["CS_DOMAIN"].ColumnName = "Computer_Domain"
($DataTable).Columns["USER_LOGGED"].ColumnName = "Last_User_Logged_In"
($DataTable).Columns["USER_DOMAIN"].ColumnName = "Last_User_Domain"
($DataTable).Columns["IS_DRIVE_ENCRYPTION_ENABLED"].ColumnName = "Encryption_Enabled"
($DataTable).Columns["PROTECTION_REASON"].ColumnName = "Encryption_Technology"
($DataTable).Columns["IS_DRIVE_PROTECTED"].ColumnName = "Is_Drive_Encrypted"
Try {
($DataTable).Columns["FIELD_10002"].ColumnName = "Asset_Dept"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2442"].ColumnName = "AD_Distinguished_Name"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2439"].ColumnName = "Windows_Local_Admins"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2432"].ColumnName = "Windows_Rogue_Local_Admins"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2430"].ColumnName = "Windows_Local_Users"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_16018"].ColumnName = "Windows_Local_Logon_Restricted"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2431"].ColumnName = "Windows_Disabled_Local_Users"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2428"].ColumnName = "AppLocker_Enforcing"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2429"].ColumnName = "Cylance_Version_Windows"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_11850"].ColumnName = "Cylance_Version_Mac"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2440"].ColumnName = "McAfee_DAT_Version_Windows"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_12297"].ColumnName = "McAfee_DAT_Version_Mac"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2433"].ColumnName = "Mac_Domain"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2446"].ColumnName = "Mac_Local_Admins"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_2434"].ColumnName = "Mac_Last_User_Domain"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_12535"].ColumnName = "Windows_Folder_Redirection"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_16376"].ColumnName = "Identity_Finder_Windows"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_16378"].ColumnName = "Identity_Finder_Mac"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_15490"].ColumnName = "Mac_Auto_Update_Check"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_15492"].ColumnName = "Mac_Auto_Update_OS"
($DataTable).Columns["MACHINE_CUSTOM_INVENTORY_0_15491"].ColumnName = "Mac_Auto_Update_Apps"
}
Catch {
    "The MACHINE_CUSTOM_INVENTORY or Asset fields do not match what is coming from KACE."
    "Please update the entries in the Rename Columns section of the script."
    Exit 1
}


# Grab WSUS data
$wsus_login = Invoke-WebRequest -Uri https://metal1.nss.udel.edu/cgi-bin/win_requests/wsus.cgi -SessionVariable WSUS_Session
$wsus_login_form = $wsus_login.Forms[0]
$wsus_login_form.fields['auth_username'] = $creds.UserName
$wsus_login_form.fields['auth_password'] = $creds.GetNetworkCredential().Password
$wsus_login_done = Invoke-WebRequest -Uri ('https://metal1.nss.udel.edu/cgi-bin/win_requests/wsus.cgi' + $form.Action) -WebSession $WSUS_Session -Method POST -Body $wsus_login_form.Fields
#     Clear credentials
$wsus_login_form = $null
#
$wsus_data = Invoke-WebRequest https://metal1.nss.udel.edu/cgi-bin/win_requests/wsus.cgi?all_computers_brief=1 -WebSession $WSUS_Session -UseBasicParsing
$wsus_data_raw = $wsus_data.RawContent
$wsus_table_raw = $wsus_data_raw.Substring($wsus_data_raw.indexof("Definition Updates.</p><table>")+30)
$wsus_table_raw = $wsus_table_raw.Substring(0, $wsus_table_raw.indexof("</table>"))
$wsus_table_raw = $wsus_table_raw -replace "<a([^>]+)>", ""
$wsus_table_raw = $wsus_table_raw -replace ".win.udel.edu", ""
$wsus_table_raw = $wsus_table_raw -replace " align=center", ""
$wsus_table_raw = $wsus_table_raw -replace "</td><td>", "</td>`n<td>"
$wsus_table_raw = $wsus_table_raw -replace "<td></td>", "<td>#NOT YET REPORTED#</td>"
ForEach($string in $wsus_table_raw){
    $table = $string.split("`n")
    $WSUSTable = @()
    $WSUSTable += ($table[1] -replace "</B></TH><TH><B>",",") -replace "</?(TR|TH|B)>"
    $WSUSTable += $table[2..($table.count-2)]|ForEach{$_ -replace "</TD><TD>","," -replace "</?T(D|R)>"}
    }
$WSUSTable = $WSUSTable -replace ",", "`n"
$WSUSTable = $WSUSTable -replace "^\s+", ""
$WSUSTable = $WSUSTable | ? {$_} #remove empty lines


# Clear Credentials
$creds = $null


# Parse Data and Calculate New Columns

#    Create new columns
Create-Column "Windows_Enterprise" "OS_Name"
Create-Column "OS_Family" "OS_Name"
Create-Column "Computer_OU" "AD_Distinguished_Name"
Create-Column "Computer_Dept" "Computer_OU"
Create-Column "WSUS_Name"
Create-Column "WSUS_IP"
Create-Column "WSUS_Last_Report"
Create-Column "WSUS_Unknown_Updates"
Create-Column "WSUS_Failed_Updates"
Create-Column "WSUS_Needed_Important_Updates"
Create-Column "WSUS_Needed_Updates"
Create-Column "WSUS_Installed_or_Not_Applicable_Updates"
Create-Column "WSUS_Reboot_Needed"
Create-Column "Is_Domain_Joined"
Create-Column "Is_Encrypted_If_Laptop"
Create-Column "Backups_or_Folder_Redirection"
Create-Column "User_Domain_Login"
Create-Column "Is_Enterprise_OS"
Create-Column "No_Rogue_Admins"
Create-Column "Cylance_Installed"
Create-Column "McAfee_Installed"
Create-Column "McAfee_Updated"
Create-Column "Auto_OS_Updates"
Create-Column "WSUS_Less_Than_10_Failed_Updates"
Create-Column "WSUS_Less_Than_10_Important_Updates_Needed"
Create-Column "Overall_Compliance_Score"

#    Move AppLocker_Enforcing column
($DataTable.Columns["AppLocker_Enforcing"]).SetOrdinal(($DataTable.Columns["No_Rogue_Admins"].Ordinal))


foreach ($Row in $DataTable.Rows) {

    # Calculate Windows_Enterprise based on OS_Name
    If ($Row["OS_Name"] -like "Mac*") {
        $Row["Windows_Enterprise"]="#N/A#"
    }
    ElseIf ($Row["OS_Name"] -like "*Enterprise*") {
        $Row["Windows_Enterprise"]="1"
    }
    Else {
        $Row["Windows_Enterprise"]="0"
    }

    # Calculate OS_Family based on OS_Name
    If ($Row["OS_Name"] -like "Mac*") {
        $Row["OS_Family"]="Mac"
    }
    Else {
        $Row["OS_Family"]="Windows"
    }

    If ($Row["Manufacturer"] -eq "ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ") {
        $Row["Manufacturer"] = "Intel Corporation"
        $Row["Model"] = ""
        $Row["Serial_Number"] = ""
    }
    ElseIf ($Row["Manufacturer"] -eq "VMware, Inc.") {
        $Row["Serial_Number"] = ""
    }
    ElseIf ($Row["Manufacturer"] -eq "System manufacturer") {
        $Row["Manufacturer"] = ""
        $Row["Model"] = ""
        $Row["Serial_Number"] = ""
    }
    ElseIf ($Row["Manufacturer"] -eq "Parallels Software International Inc.") {
        $Row["Manufacturer"] = "Parallels"
        $Row["Serial_Number"] = ""
    }

    #    Clean up Windows specific custom inventory columns
    If ($Row["OS_Family"] -eq "Windows") {
        # Clean up Windows_Local_Admins
        If ($Row["Windows_Local_Admins"] -like "Alias name     Administrators<br/>*") {
            $RawWinLocalAdminText = $Row["Windows_Local_Admins"]
            $RawWinLocalAdminText = $RawWinLocalAdminText.Replace("Alias name     Administrators<br/>Comment        Administrators have complete and unrestricted access to the computer/domain<br/><br/>Members<br/><br/>-------------------------------------------------------------------------------<br/>", "")
            $RawWinLocalAdminText = $RawWinLocalAdminText.Replace("<br/>The command completed successfully.<br/><br/>", "")
            $WinRogueAdminText = $RawWinLocalAdminText
            $RawWinLocalAdminText = $RawWinLocalAdminText | Clean-Text
            $Row["Windows_Local_Admins"] = $RawWinLocalAdminText
            # Calculate Windows_Rogue_Local_Admins
            $WinRogueAdminText = $WinRogueAdminText | Clean-Text
            $WinRogueAdminText = $WinRogueAdminText -replace "Administrator\W", ""
            $WinRogueAdminText = $WinRogueAdminText -replace "(?<!-)Administrator", ""
            $WinRogueAdminText = $WinRogueAdminText.Replace("WIN\Domain Admins", "")
            $WinRogueAdminText = $WinRogueAdminText.Replace("WIN\CSSM-Users-Admins", "")
            $WinRogueAdminText = $WinRogueAdminText.Replace("WIN\CSSM_Admins", "")
            $WinRogueAdminText = $WinRogueAdminText.Replace("WIN\CSS_Admins", "")
            $WinRogueAdminText = $WinRogueAdminText -replace "WIN\\.*\-.*Admin.*", ""
            $WinRogueAdminText = $WinRogueAdminText | Clean-Text
            $Row["Windows_Rogue_Local_Admins"] = $WinRogueAdminText
        }
        ElseIf ($Row["Windows_Local_Admins"] -like "*is not recognized as an internal*") {
            $Row["Windows_Local_Admins"] = "#UNKNOWN#"
            $Row["Windows_Rogue_Local_Admins"] = "#UNKNOWN#"
            $Row["Windows_Local_Users"] = "#UNKNOWN#"
            $Row["Windows_Disabled_Local_Users"] = "#UNKNOWN#"
            $Row["Cylance_Version_Windows"] = "#UNKNOWN#"
        }
        ElseIf ($Row["Windows_Local_Admins"] -ne [System.DBNull]::Value) {
            $RawWinLocalAdminText = $Row["Windows_Local_Admins"]
            $RawWinLocalAdminText = $RawWinLocalAdminText | Clean-Text
            $Row["Windows_Local_Admins"] = $RawWinLocalAdminText
            }
        # Clean up Windows_Rogue_Local_Admins column
        If ($Row["Windows_Rogue_Local_Admins"] -ne [System.DBNull]::Value) {
            $RawWinRogueLocalAdminText = $Row["Windows_Rogue_Local_Admins"]
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText | Clean-Text
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText.Replace("WIN\Domain Admins", "")
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText.Replace("DVSportSupport", "")
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText.Replace("LogMeInRemoteUser", "")
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText.Replace("WIN\PROV-Users-IT", "")
            $RawWinRogueLocalAdminText = $RawWinRogueLocalAdminText | Clean-Text
            $Row["Windows_Rogue_Local_Admins"] = $RawWinRogueLocalAdminText
        }
        # Clean up Windows_Local_Users column
        If ($Row["Windows_Local_Users"] -ne [System.DBNull]::Value) {
            $RawWinLocalUsersText = $Row["Windows_Local_Users"]
            $RawWinLocalUsersText = $RawWinLocalUsersText | Clean-Text
            $Row["Windows_Local_Users"] = $RawWinLocalUsersText
        }
        # Clean up Windows_Local_Logon_Restricted column
        If ($Row["Windows_Local_Logon_Restricted"] -ne [System.DBNull]::Value) {
            $RawWinLocalLogonText = $Row["Windows_Local_Logon_Restricted"]
            $RawWinLocalLogonText = $RawWinLocalLogonText | Clean-Text
            $RawWinLocalLogonText = $RawWinLocalLogonText.Replace("DisplayName REG_SZ ", "")
            If ($RawWinLocalLogonText -like "*is not recognized*"){
                $Row["Windows_Local_Logon_Restricted"] = [System.DBNull]::Value
            }
            Else {
                
                $Row["Windows_Local_Logon_Restricted"] = $RawWinLocalLogonText.Split()[0]
            }
        }
        # Clean up Windows_Disabled_Local_Users column
        If ($Row["Windows_Disabled_Local_Users"] -ne [System.DBNull]::Value) {
            $RawWinLocalDisabledUsersText = $Row["Windows_Disabled_Local_Users"]
            $RawWinLocalDisabledUsersText = $RawWinLocalDisabledUsersText.Replace("No Instance(s) Available.", "")
            $RawWinLocalDisabledUsersText = $RawWinLocalDisabledUsersText | Clean-Text
            If ($RawWinLocalDisabledUsersText -like "*ERROR:*") {$RawWinLocalDisabledUsersText = "#UNKNOWN#"}
            $Row["Windows_Disabled_Local_Users"] = $RawWinLocalDisabledUsersText
        }
        # Clean up Cylance_Version_Windows column
        If ($Row["Cylance_Version_Windows"] -ne [System.DBNull]::Value) {
            $CylanceVersionText = $Row["Cylance_Version_Windows"]
            $CylanceVersionText = $CylanceVersionText.Replace("No Instance(s) Available.", "")
            $CylanceVersionText = $CylanceVersionText | Clean-Text
            If ($CylanceVersionText -like "*ERROR:*") {$CylanceVersionText = "#UNKNOWN#"}
            $Row["Cylance_Version_Windows"] = $CylanceVersionText
        }
        # Clean up Identity_Finder_Windows column
        If ($Row["Identity_Finder_Windows"] -ne [System.DBNull]::Value) {
            $IDFinderWinText = $Row["Identity_Finder_Windows"]
            If ($IDFinderWinText.IndexOf("Identity Finder") -ne -1) {
                $IDFinderWinText = $IDFinderWinText.Replace("Identity Finder ", "")
                $IDFinderWinText = $IDFinderWinText | Clean-Text
                $Row["Identity_Finder_Windows"] = $IDFinderWinText
            }
            Else {
                $Row["Identity_Finder_Windows"] = [System.DBNull]::Value
            }
        }
        If ($Row["Windows_Enterprise"] -ne "1" -or $Row["Computer_Domain"] -ne "win.udel.edu") {
            $Row["AppLocker_Enforcing"] = "0"
        }
        # Normalize Windows_Folder_Redirection
        If ($Row["Windows_Folder_Redirection"] -ne [System.DBNull]::Value) {
            $RawWinFolderRedirectText = $Row["Windows_Folder_Redirection"] | Clean-Text
            $FolderRedirectMatches = (Select-String -Pattern "win\.udel\.edu" -InputObject $RawWinFolderRedirectText -AllMatches).Matches.Count
            If ($FolderRedirectMatches -eq 2) {
                $Row["Windows_Folder_Redirection"] = "1"
            }
            Else {
                $Row["Windows_Folder_Redirection"] = "0"
            }
        }
        Else {
            $Row["Windows_Folder_Redirection"] = "0"
        }
    }

    #    Clean up inventory fields for Macs
    If ($Row["OS_Family"] -eq "Mac") {
        # Clean up line breaks and spaces for Mac custom inventory columns
        If ($Row["Mac_Domain"] -ne [System.DBNull]::Value) {
            $MacDomainText = $Row["Mac_Domain"]
            $MacDomainText = $MacDomainText | Clean-Text
            $Row["Mac_Domain"] = $MacDomainText
        }
        If ($Row["Mac_Last_User_Domain"] -ne [System.DBNull]::Value) {
            $MacLastUserDomainText = $Row["Mac_Last_User_Domain"]
            $MacLastUserDomainText = $MacLastUserDomainText | Clean-Text
            $Row["Mac_Last_User_Domain"] = $MacLastUserDomainText
        }
        If ($Row["Mac_Local_Admins"] -ne [System.DBNull]::Value) {
            $MacLocalAdminsText = $Row["Mac_Local_Admins"]
            $MacLocalAdminsText = $MacLocalAdminsText | Clean-Text
            $Row["Mac_Local_Admins"] = $MacLocalAdminsText
            $Row["Windows_Local_Admins"] = $MacLocalAdminsText
            $MacLocalAdminsText = $MacLocalAdminsText.Replace("root", "")
            $MacLocalAdminsText = $MacLocalAdminsText | Clean-Text
            $Row["Windows_Rogue_Local_Admins"] = $MacLocalAdminsText
        }
        If ($Row["Mac_Auto_Update_Check"] -ne [System.DBNull]::Value) {
            If ($Row["Mac_Auto_Update_Check"] -like "*is on*") {
                $Row["Mac_Auto_Update_Check"] = "1"
            }
            Else {
                $Row["Mac_Auto_Update_Check"] = "0"
            }
        }
        Else {
            $Row["Mac_Auto_Update_Check"] = "0"
        }
        If ($Row["Mac_Auto_Update_OS"] -ne [System.DBNull]::Value) {
            If ($Row["Mac_Auto_Update_OS"] -like "*1*") {
                $Row["Mac_Auto_Update_OS"] = "1"
            }
            Else {
                $Row["Mac_Auto_Update_OS"] = "0"
            }
        }
        Else {
            $Row["Mac_Auto_Update_OS"] = "0"
        }
        If ($Row["Mac_Auto_Update_Apps"] -ne [System.DBNull]::Value) {
            If ($Row["Mac_Auto_Update_Apps"] -like "*1*") {
                $Row["Mac_Auto_Update_Apps"] = "1"
            }
            Else {
                $Row["Mac_Auto_Update_Apps"] = "0"
            }
        }
        Else {
            $Row["Mac_Auto_Update_Apps"] = "0"
        }
        If ($Row["Mac_Auto_Update_Check"] -eq "1" -and $Row["Mac_Auto_Update_OS"] -eq "1") {
            $Row["Auto_OS_Updates"] = "1"
        }
        Else {
            $Row["Auto_OS_Updates"] = "0"
        }

        #    Set Windows specific custom inventory columns for Macs
    
        $Row["Windows_Local_Users"] = "#N/A#"
        $Row["Windows_Disabled_Local_Users"] = "#N/A#"
        $Row["AppLocker_Enforcing"] = "1"
        If ($Row["Cylance_Version_Mac"] -ne [System.DBNull]::Value) {
            $Row["Cylance_Version_Windows"] = $Row["Cylance_Version_Mac"] | Clean-Text
        }
        Else {
            $Row["Cylance_Version_Windows"] = "#UNKNOWN#"
        }
        If ($Row["McAfee_DAT_Version_Mac"] -ne [System.DBNull]::Value) {
            $Row["McAfee_DAT_Version_Windows"] = $Row["McAfee_DAT_Version_Mac"] | Clean-Text
        }
        Else {
            $Row["McAfee_DAT_Version_Windows"] = "#UNKNOWN#"
        }
        If ($Row["Identity_Finder_Mac"] -ne [System.DBNull]::Value) {
            $Row["Identity_Finder_Windows"] = $Row["Identity_Finder_Mac"] | Clean-Text
        }
        $Row["Computer_Domain"] = $Row["Mac_Domain"]
        $Row["Last_User_Domain"] = $Row["Mac_Last_User_Domain"]
        $Row["WSUS_Name"] = "#N/A#"
        $Row["WSUS_IP"] = "#N/A#"
        $Row["WSUS_Last_Report"] = "#N/A#"
        $Row["WSUS_Unknown_Updates"] = "#N/A#"
        $Row["WSUS_Failed_Updates"] = "#N/A#"
        $Row["WSUS_Needed_Important_Updates"] = "#N/A#"
        $Row["WSUS_Needed_Updates"] = "#N/A#"
        $Row["WSUS_Installed_or_Not_Applicable_Updates"] = "#N/A#"
        $Row["WSUS_Reboot_Needed"] = "#N/A#"
    }


    # Find AD DN and calculate immediate OU and parent dept (ATHL, EXEC, HR, FBAA, PROV, etc.)
    If ($Row["Computer_Domain"] -eq "win.udel.edu") {
        $SystemName = $Row["System_Name"]
        Try {
        $Row["AD_Distinguished_Name"] = (Get-ADComputer $SystemName).DistinguishedName
        }
        Catch {
        $Row["AD_Distinguished_Name"] = "#ERROR: SYSTEM NAME NOT FOUND IN AD#"
        }
    }
    Else {
        $Row["AD_Distinguished_Name"] = [System.DBNull]::Value
    }
    If ($Row["AD_Distinguished_Name"] -ne [System.DBNull]::Value -and $Row["AD_Distinguished_Name"] -ne "#ERROR: NOT FOUND IN AD#") {
        $computerdn = $Row["AD_Distinguished_Name"]
        $computerou = (($computerdn.Remove($computerdn.IndexOf(",OU=University,DC=win,DC=udel,DC=edu"))).Split(',')[1])
        $computerou = $computerou.Replace("OU=","")
        $Row["Computer_OU"] = $computerou
    }
    ElseIf ($Row["Computer Domain"] -ne "win.udel.edu") {
        $Row["Computer_OU"] = "#UNKNOWN#"
    }
    If ($Row["Computer_OU"] -eq "#UNKNOWN#" -and $Row["Asset_Dept"] -ne [System.DBNull]::Value) {
        $Row["Computer_OU"] = $Row["Asset_Dept"]
        $computerou = $Row["Asset_Dept"]
    }
    If ($Row["Computer_OU"] -ne "#UNKNOWN#") {
        $computerprefix = (($computerou.Substring($computerou.Indexof("=")+1)).Split('-')[0])
        $Row["Computer_Dept"] = $computerprefix
    }
    ElseIf ($Row["System_Name"] -match "^([A-Za-z]){2,4}-.*") {
        $SystemName = $Row["System_Name"]   
        $computerprefix = ($SystemName.Split('-')[0])
        $computerprefix = $computerprefix -replace "SFS","FBAA"
        $computerprefix = $computerprefix.ToUpper()
        $Row["Computer_Dept"] = $computerprefix
    }
    Else {
        $Row["Computer_Dept"] = "#UNKNOWN#"
    }
    # Cleanups
    If ($Row["OS_Installed_Date"] -ne [System.DBNull]::Value) {
        $OSInstalledDateText = $Row["OS_Installed_Date"]
        $OSInstalledDateText = $OSInstalledDateText.Split("T",2)[0]
        $Row["OS_Installed_Date"] = $OSInstalledDateText
    }
    Else {
        $Row["OS_Installed_Date"] = "#UNKNOWN#"
    }
    If ($Row["Uptime"] -ne [System.DBNull]::Value) {
        $UptimeText = $Row["Uptime"]
        $UptimeText = $UptimeText.Replace(",", "")
        $Row["Uptime"] = $UptimeText
    }
    Else {
        $Row["Uptime"] = "#UNKNOWN#"
    }
    #    Set certain columns to #UNKNOWN# if still null
    If ($Row["Windows_Local_Admins"] -eq [System.DBNull]::Value) {
        $Row["Windows_Local_Admins"] = "#UNKNOWN#"
        $Row["Windows_Rogue_Local_Admins"] = "#UNKNOWN#"
        $Row["Windows_Local_Users"] = "#UNKNOWN#"
        $Row["Windows_Disabled_Local_Users"] = "#UNKNOWN#"
    }
    If ($Row["AppLocker_Enforcing"] -eq [System.DBNull]::Value) {
        $Row["AppLocker_Enforcing"] = "0"
    }
    If ($Row["McAfee_DAT_Version_Windows"] -eq [System.DBNull]::Value) {
        $Row["McAfee_DAT_Version_Windows"] = "#UNKNOWN#"
    }
    If ($Row["Cylance_Version_Windows"] -eq [System.DBNull]::Value) {
        $Row["Cylance_Version_Windows"] = "#UNKNOWN#"
    }
    # Populate Is_Domain_Joined
    If ($Row["Computer_Domain"] -eq "win.udel.edu") {
        $Row["Is_Domain_Joined"] = "1"
    }
    Else {
        $Row["Is_Domain_Joined"] = "0"
    }
    # Populate User_Domain_Login
    # Equals 1 if the user is on WIN and logging in as WIN\ or if the user is not on the domain
    # Equals 0 only when user is on WIN but logging in with a local account
    If ($Row["Is_Domain_Joined"] -eq "1") {
        If ($Row["Last_User_Domain"] -eq "WIN") {
            $Row["User_Domain_Login"] = "1"
        }
        Else {
            $Row["User_Domain_Login"] = "0"
        }
    }
    Else {
        $Row["User_Domain_Login"] = "0"
    }
    # Populate Is_Encrypted_If_Laptop
    If ($Row["Chassis_Type"] -ne "laptop") {
        $Row["Is_Encrypted_If_Laptop"] = "1"
    }
    ElseIf ($Row["Chassis_Type"] -eq "laptop" -and $Row["Is_Drive_Encrypted"] -eq "True") {
        $Row["Is_Encrypted_If_Laptop"] = "1"
    }
    Else {
        $Row["Is_Encrypted_If_Laptop"] = "0"
    }
    # Populate Backups_or_Folder_Redirection
    If ($Row["OS_Family"] -eq "Mac") {
        $Row["Backups_or_Folder_Redirection"] = "0"
    }
    ElseIf ($Row["Computer_OU"] -like "*Shared*") {
        $Row["Backups_or_Folder_Redirection"] = "1"
    }
    ElseIf ($Row["Windows_Folder_Redirection"] -eq "1") {
        $Row["Backups_or_Folder_Redirection"] = "1"
    }
    Else {
        $Row["Backups_or_Folder_Redirection"] = "0"
    }
    # Populate Is_Enterprise_OS
    If ($Row["Windows_Enterprise"] -eq "0") {
        $Row["Is_Enterprise_OS"] = "0"
    }
    Else {
        $Row["Is_Enterprise_OS"] = "1"
    }
    # Populate No_Rogue_Admins
    If ($Row["Windows_Rogue_Local_Admins"] -eq [System.DBNull]::Value -or $Row["Windows_Rogue_Local_Admins"] -eq $null) {
        $Row["No_Rogue_Admins"] = "1"
    }
    Else {
        $Row["No_Rogue_Admins"] = "0"
    }
    # Populate Cylance_Installed
    If ($Row["Cylance_Version_Windows"] -match "^([0-9]+\.).*" ) {
        $Row["Cylance_Installed"] = "1"
    }
    ElseIf ($Row["Computer_OU"] -like "*Shared*") {
        $Row["Cylance_Installed"] = "1"
    }
    Else {
        $Row["Cylance_Installed"] = "0"
    }
    # Populate McAfee_Updated
    $McAfeeDate = $Row["McAfee_DAT_Version_Windows"]
    If ($McAfeeDate -ne "#UNKNOWN#" -and $McAfeeDate -ne "#N/A#") {
        $Row["McAfee_Installed"] = "1"
        $McAfeeDay = $McAfeeDate.Split("/")[2] -as [int]
        $McAfeeYear = $McAfeeDate.Split("/")[0] -as [int]
        $McAfeeMonth = $McAfeeDate.Split("/")[1] -as [int]
        If ($currentday -ge 8 -and $McAfeeMonth -eq $currentmonth -and $McAfeeDay -ge ($currentday - 7) -and $McAfeeYear -eq $currentyear) {
            $Row["McAfee_Updated"] = "1"
        }
        ElseIf ($currentday -le 7 -and $McAfeeMonth -ge ($currentmonth - 1) -and ($McAfeeDay -ge 21 -or $McAfeeDay -le 7) -and $McAfeeYear -ge $currentyear) {
            $Row["McAfee_Updated"] = "1"
        }
        ElseIf ($currentday -le 7 -and $currentmonth -eq 1 -and $McAfeeMonth -eq 12 -and $McAfeeDay -ge 21 -and $McAfeeYear -eq ($currentyear - 1)) {
            $Row["McAfee_Updated"] = "1"
        }
        Else {
            $Row["McAfee_Updated"] = "0"
        }
    }
    Else {
        $Row["McAfee_Installed"] = "0"
        $Row["McAfee_Updated"] = "0"
    }

# Populate WSUS columns
    If ($Row["OS_Family"] -eq "Windows") {
        $KACEname = $Row["System_Name"].ToLower()
        $WSUSIndexNum = [array]::IndexOf($WSUSTable, $KACEname)
        If ($WSUSIndexNum -ne -1) {
            $Row["WSUS_Name"] = $WSUSTable[$WSUSIndexNum]
            $Row["WSUS_IP"] = $WSUSTable[$WSUSIndexNum + 1]
            $Row["WSUS_Last_Report"] = $WSUSTable[$WSUSIndexNum + 2] -as [DateTime]
            $Row["WSUS_Unknown_Updates"] = $WSUSTable[$WSUSIndexNum + 3]
            $Row["WSUS_Failed_Updates"] = $WSUSTable[$WSUSIndexNum + 4]
            $Row["WSUS_Needed_Important_Updates"] = $WSUSTable[$WSUSIndexNum + 5]
            $Row["WSUS_Needed_Updates"] = $WSUSTable[$WSUSIndexNum + 6]
            $Row["WSUS_Installed_or_Not_Applicable_Updates"] = $WSUSTable[$WSUSIndexNum + 7]
            $Row["WSUS_Reboot_Needed"] = $WSUSTable[$WSUSIndexNum + 8]
        }
        Else {
            $Row["WSUS_Name"] = "#No WSUS#"
            $Row["WSUS_IP"] = "#No WSUS#"
            $Row["WSUS_Last_Report"] = "#No WSUS#"
            $Row["WSUS_Unknown_Updates"] = "#No WSUS#"
            $Row["WSUS_Failed_Updates"] = "#No WSUS#"
            $Row["WSUS_Needed_Important_Updates"] = "#No WSUS#"
            $Row["WSUS_Needed_Updates"] = "#No WSUS#"
            $Row["WSUS_Installed_or_Not_Applicable_Updates"] = "#No WSUS#"
            $Row["WSUS_Reboot_Needed"] = "#No WSUS#"
        }
    }


    # Populate Auto_OS_Updates
    If ($Row["WSUS_Name"] -eq "#No WSUS#") {
        $Row["Auto_OS_Updates"] = "0"
    }
    Else {
        $Row["Auto_OS_Updates"] = "1"
    }

    # Populate WSUS_Less_Than_10_Failed_Updates
    If ($Row["OS_Family"] -eq "Windows" -and $Row["Auto_OS_Updates"] -eq "1") {
        $numFailedWSUSUpdates = $Row["WSUS_Failed_Updates"] -as [int]
        If ($numFailedWSUSUpdates -lt 10) {
            $Row["WSUS_Less_Than_10_Failed_Updates"] = "1"
        }
        Else {
            $Row["WSUS_Less_Than_10_Failed_Updates"] = "0"
        }
    }
    ElseIf ($Row["OS_Family"] -eq "Mac") {
        $Row["WSUS_Less_Than_10_Failed_Updates"] = "1"
    }
    Else {
        $Row["WSUS_Less_Than_10_Failed_Updates"] = "0"
    }

#    Populate WSUS_Less_Than_10_Important_Updates_Needed
    If ($Row["OS_Family"] -eq "Windows" -and $Row["Auto_OS_Updates"] -eq "1") {
        $numNeedImpWSUSUpdates = $Row["WSUS_Failed_Updates"] -as [int]
        If ($numNeedImpWSUSUpdates -lt 10) {
            $Row["WSUS_Less_Than_10_Important_Updates_Needed"] = "1"
        }
        Else {
            $Row["WSUS_Less_Than_10_Important_Updates_Needed"] = "0"
        }
    }
    ElseIf ($Row["OS_Family"] -eq "Mac") {
        $Row["WSUS_Less_Than_10_Important_Updates_Needed"] = "1"
    }
    Else {
        $Row["WSUS_Less_Than_10_Important_Updates_Needed"] = "0"
    }

#    Populate Overall_Compliance_Score
    $compliance_score = 0
    If ($Row["Is_Domain_Joined"] -eq "1") {
        $compliance_score += 20
    }
    If ($Row["Is_Encrypted_If_Laptop"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["Is_Enterprise_OS"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["No_Rogue_Admins"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["AppLocker_Enforcing"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["Cylance_Installed"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["McAfee_Installed"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["Auto_OS_Updates"] -eq "1") {
        $compliance_score += 10
    }
    If ($Row["Backups_or_Folder_Redirection"] -eq "1") {
        $compliance_score += 10
    }
    $Row["Overall_Compliance_Score"] = [int]$compliance_score
}


$DataTable.Columns.Remove("Cylance_Version_Mac")
$DataTable.Columns.Remove("McAfee_DAT_Version_Mac")
$DataTable.Columns.Remove("Asset_Dept")
$DataTable.Columns.Remove("Mac_Domain")
$DataTable.Columns.Remove("Mac_Last_User_Domain")
$DataTable.Columns.Remove("Mac_Local_Admins")
$DataTable.Columns.Remove("Identity_Finder_Mac")
($DataTable).Columns["Windows_Local_Admins"].ColumnName = "Local_Admins"
($DataTable).Columns["Windows_Rogue_Local_Admins"].ColumnName = "Rogue_Local_Admins"
($DataTable).Columns["Cylance_Version_Windows"].ColumnName = "Cylance_Version"
($DataTable).Columns["Identity_Finder_Windows"].ColumnName = "Identity_Finder_Version"
($DataTable).Columns["McAfee_DAT_Version_Windows"].ColumnName = "McAfee_DAT_Version"
$DataTable.AcceptChanges()


# Populate Hash Table
$objTable = $DataTable | Select * -ExcludeProperty RowError, RowState, Table, ItemArray, HasErrors


# Export Hash Table to file
#$objTable | Export-CSV $AttachmentPath -NoTypeInformation
$PivotData_Hash = [ordered]@{System_Name='count'; Overall_Compliance_Score='average'; Is_Domain_Joined='sum'; Backups_or_Folder_Redirection='sum'; Is_Encrypted_If_Laptop='sum'; Is_Enterprise_OS='sum'; No_Rogue_Admins='sum'; AppLocker_Enforcing='sum'; Cylance_Installed='sum'; McAfee_Installed='sum'; Auto_OS_Updates='sum'}

If ($send_email -eq 0) {
    $objTable | Export-Excel -Path $AttachmentPath -WorkSheetName "Data" -BoldTopRow -AutoSize -FreezeTopRowFirstColumn -Show -IncludePivotTable -PivotRows Computer_Dept, Computer_OU -PivotData $PivotData_Hash -PivotDataToColumn -Numberformat "0"
}
Else {
    $objTable | Export-Excel -Path $AttachmentPath -WorkSheetName "Data" -BoldTopRow -AutoSize -FreezeTopRowFirstColumn -IncludePivotTable -PivotRows Computer_Dept, Computer_OU -PivotData $PivotData_Hash -PivotDataToColumn -Numberformat "0"
}

# Send SMTP Message
If ($send_email -eq 1) {
    Send-MailMessage -To $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -From $From -Attachments $AttachmentPath
}


# Postliminaries
$enddate = Get-Date
"Processing ended:" + $enddate
$scripttime = [math]::Round(($enddate - $startdate).TotalMinutes, 2)
If ($scriptTime -lt 2) {
    $scripttime = [math]::Round(($enddate - $startdate).TotalSeconds, 0)
    "Total script execution time: " + $scripttime + " seconds"
}
Else {
    "Total script execution time: " + $scripttime + " minutes"