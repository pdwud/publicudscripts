function Register-UDNetwork {
    [cmdletbinding()]
    param (
        [parameter(Mandatory,ValueFromPipelineByPropertyName)]
        $MACAddress,
        [parameter(Mandatory,ValueFromPipelineByPropertyName)]
        $Description,
        [parameter(Mandatory)]
        [ValidateNotNullorEmpty()]
        [PSCredential]
        $creds
    )

    Begin {
        Try {
            $udnet_login = Invoke-WebRequest -Uri 'https://www1.nss.udel.edu/cgi-bin/auth/network?UDAUTHTYPE=LOGIN' -SessionVariable UDNetwork_Session
            $udnet_login_form = $udnet_login.Forms[0]
            $udnet_login_form.fields['UDLOGIN'] = $creds.UserName
            $udnet_login_form.fields['UDPASS'] = $creds.GetNetworkCredential().Password
            $udnet_login_done = Invoke-WebRequest -Uri 'https://www1.nss.udel.edu/cgi-bin/auth/network?UDAUTHTYPE=LOGIN' -WebSession $UDNetwork_Session -Method POST -Body $udnet_login_form.Fields
        }
        Catch {
            Write-Error "Failed to login"
        }
        # Clear credentials
        $udnet_login_form = $null
        Clear-Item Variable:creds
    }

    Process {
        Try {
            $udnet_register = Invoke-WebRequest -Uri 'https://www1.nss.udel.edu/cgi-bin/auth/network?CONTROL=IPCHECK_MANUAL' -WebSession $UDNetwork_Session
            $udnet_register_form = $udnet_register.Forms[0]
            $udnet_register_form.fields['MAN_MACADDR'] = $MACaddress
            $udnet_register_form.fields['DESCRIPTION'] = $Description
            $udnet_register_submit = Invoke-WebRequest -Uri 'https://www1.nss.udel.edu/cgi-bin/auth/network?CONTROL=IPCHECK_MANUAL' -WebSession $UDNetwork_Session -Method POST -Body $udnet_register_form.Fields
        }
        Catch {
            Write-Error "Failed to register $Description - $MACaddress"
        }
        Start-Sleep -Seconds 1
    }
}